import { useState, useEffect, useContext } from 'react';

import { Form, Button } from 'react-bootstrap';

import {Navigate, useNavigate} from 'react-router-dom';

import UserContext from '../UserContext';

import Swal from 'sweetalert2';

export default function Register() {

    const {user} = useContext(UserContext);

    const navigate = useNavigate();

    // State hooks to store the values of the input fields
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [verifyPassword, setVerifyPassword] = useState('');
    // State to determine whether submit button will be enabled or not
    const [isActive, setIsActive] = useState(false);

    // Function to simulate user registration
    // function registerUser(e) {
    //     // Prevents page from reloading
    //     e.preventDefault();

    //     // Clear input fields
    //     setEmail('');
    //     setPassword1('');
    //     setPassword2('');

    //     alert('Thank you for registering!');
    // };

    // Refactored user registration
    async function registerUser(e) {
        // Prevents page from reloading
        e.preventDefault();
        
    // Checking if email exist within database
        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            if(data) {
                Swal.fire({
                title: "Duplicate email found",
                icon: "error",
                text: "Please provide a different email."
                })
            } else {
                fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        mobileNo: mobileNo,
                        password: password
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data);
                    if(typeof data !== "undefined") {
                        Swal.fire({
                        title: "Registration successful",
                        icon: "success",
                        text: "Welcome to Zuitt!"
                        });

                        // Clear input fields
                        setFirstName('');
                        setLastName('');
                        setMobileNo('');
                        setEmail('');
                        setPassword('');
                        setVerifyPassword('');
                        
                        navigate("/login");
                    } else {
                        Swal.fire({
                            title: "Error on registration",
                            icon: "error",
                            text: "Try again later."
                        })
                    }
                })
            }
        })

        


        
    };

    

    // useEffect(() => {
    //     // Validation to enable submit button when all fields are populated and both passwords match
    //     if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)) {
    //         setIsActive(true);
    //     } else {
    //         setIsActive(false);
    //     }



    // Dependencies
    // No dependencies - effect function will run every time component renders
    // With dependency (empty array) - effect function will only run (one time) when the component renders
    // With dependencies - effect function will run anytime one of the values in the array of dependencies changes
    // }, [email, password1, password2])

    // Refactored useEffect()
    useEffect(() => {
        // Validation to enable submit button when all fields are populated and both passwords match
        if((firstName !== '' && lastName !== '' && email !== '' && password !== '' && verifyPassword !== '') && (password === verifyPassword) && mobileNo.length >= 11) {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [firstName, lastName, mobileNo, email, password, verifyPassword])

    // return (
    //     (user.id !== null) ?
    //     <Navigate to="/courses" />
    //     :
    //     <Form onSubmit={(e) => registerUser(e)}>
    //         <Form.Group controlId="userEmail">
    //             <Form.Label>Email address</Form.Label>
    //             <Form.Control 
	//                 type="email" 
	//                 placeholder="Enter email"
    //                 value={email}
    //                 onChange={e => setEmail(e.target.value)} 
	//                 required
    //             />
    //             <Form.Text className="text-muted">
    //                 We'll never share your email with anyone else.
    //             </Form.Text>
    //         </Form.Group>

    //         <Form.Group controlId="password1">
    //             <Form.Label>Password</Form.Label>
    //             <Form.Control 
	//                 type="password" 
	//                 placeholder="Password"
    //                 value={password1}
    //                 onChange={e => setPassword1(e.target.value)} 
	//                 required
    //             />
    //         </Form.Group>

    //         <Form.Group controlId="password2">
    //             <Form.Label>Verify Password</Form.Label>
    //             <Form.Control 
	//                 type="password" 
	//                 placeholder="Verify Password"
    //                 value={password2}
    //                 onChange={e => setPassword2(e.target.value)} 
	//                 required
    //             />
    //         </Form.Group>
    //         {/*conditionally render submit button based on "isActive" state*/}
    //         { isActive ?
    //         <Button variant="primary" type="submit" id="submitBtn">
    //             Submit
    //         </Button>
    //         :
    //         <Button variant="danger" type="submit" id="submitBtn">
    //             Submit
    //         </Button>
    //         }
            
    //     </Form>
    // )

    // Refactored return()
    return (
        (user.id !== null) ?
        <Navigate to="/courses" />
        :
        <Form onSubmit={(e) => registerUser(e)}>
            <Form.Group controlId="firstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control type="text" placeholder="Enter your First Name" value={firstName} onChange={e => setFirstName(e.target.value)} required />
            </Form.Group>

            <Form.Group controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control type="text" placeholder="Enter your Last Name" value={lastName} onChange={e => setLastName(e.target.value)} required />
            </Form.Group>

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={e => setEmail(e.target.value)} 
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="mobileNo">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control type="number" placeholder="Enter your mobile number" value={mobileNo} onChange={e => setMobileNo(e.target.value)} required />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password}
                    onChange={e => setPassword(e.target.value)} 
                    required
                />
            </Form.Group>

            <Form.Group controlId="verifyPassword">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Verify Password"
                    value={verifyPassword}
                    onChange={e => setVerifyPassword(e.target.value)} 
                    required
                />
            </Form.Group>
            {/*conditionally render submit button based on "isActive" state*/}
            { isActive ?
            <Button variant="primary" type="submit" id="submitBtn">
                Submit
            </Button>
            :
            <Button variant="danger" type="submit" id="submitBtn">
                Submit
            </Button>
            }
            
        </Form>
    )

}
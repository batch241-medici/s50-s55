// import from react
import {useState, useEffect} from 'react';

import {Link} from 'react-router-dom';

import {Card, Button} from 'react-bootstrap';

export default function CourseCard({course}) {

	// const {name, description, price} = course;

	const {name, description, price, _id} = course;

	// Use the state hook for this component to be able to store its state
	// States are used to keep track off information related to individual components
	/*
		Syntax:
		const [getter, setter] = useState(initialGetterValue)
	*/
	// const [count, setCount] = useState(0);
	// console.log(useState);
	// // S51 Activity
	// const [seat, setSeat] = useState(30);
	// const [isOpen, setIsOpen] = useState(true);

	// function that keeps track of the enrollees for a course
	// function enroll() {
	// 	setCount(count + 1);
	// 	setSeat(seat - 1);
	// 	/*if(seat === 0) {
	// 		setCount(count + 0);
	// 		setSeat(seat - 0);
	// 		return alert("No more seats.")
	// 	}*/
	// 	/*if(seats === 0) {
	// 		alert("No more seats.")
	// 		document.querySelector(`#btn-enroll-${id}`).setAttribute('disabled', true);
	// 	}*/
	// }
	// useEffect() - will allow us to execute a function if the value of seats state changes
	// useEffect(() => {
	// 	if(seat === 0) {
	// 		setIsOpen(false);
	// 		document.querySelector(`#btn-enroll-${id}`).setAttribute('disabled', true);
	// 	}
	// 	// will run anytime one of the values in the array of the dependencies changes
	// }, [seat])

	return (
		<Card>
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>Php {price}</Card.Text>
				{/*<Card.Subtitle>Enrollees:</Card.Subtitle>
				<Card.Text>{count} Enrollees</Card.Text>*/}
				{/*<Button variant="primary">Enroll</Button>*/}
				{/*<Button className="bg-primary" onClick={enroll}>Enroll</Button>*/}
				{/*<Button id={`btn-enroll-${id}`} className="bg-primary" onClick={enroll}>Enroll</Button>*/}
				<Button className="bg-primary" as={Link} to={`/courses/${_id}`}>Details</Button>
			</Card.Body>
		</Card>

	)
}
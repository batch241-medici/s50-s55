/*import {Link} from 'react-router-dom';
import {Navigate} from 'react-router-dom';

import {Row, Col, Button} from 'react-bootstrap';

export default function ErrorPage() {

	return (
		<Row>
	    	<Col className="p-5">
	            <h1>Page not found</h1>
	            <p>Go back to the <a href="/">homepage</a>.</p>
	            <Button variant="primary" as={Link} to="/">Home</Button>
	        </Col>
    	</Row>
	)
}*/

import Banner from '../components/Banner';

export default function Error() {
	const data = {
		title: "Error 404 - Page Not Found",
		content: "The page you are looking for cannot be found.",
		destination: "/",
		label: "Back to Home"
	}
	return (
		<Banner data={data}/>
	)
}